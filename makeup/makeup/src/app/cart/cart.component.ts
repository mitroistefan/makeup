import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ShoppingCart } from '../entities/shopping.cart';
import { CartService } from '../services/cart.service';
import { Product } from '../entities/product';
import { ShoppingCartItem } from '../entities/shopping.cart.item';

import { ProductService } from '../services/product.service';

import { Subscription } from "rxjs/Subscription";




@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public cart: Observable<ShoppingCart>;
  private cartSubscription: Subscription;
  public itemCount: number;
  private products: Product[];
  private cartItems: ShoppingCartItem[];

  constructor(private cartService: CartService,
    private productsService: ProductService) { }


  public ngOnInit(): void {
    this.cart = this.cartService.get();
    this.cartSubscription = this.cart.subscribe((cart) => {
      this.itemCount = cart.itemCount;
      this.productsService.getProducts().subscribe((products) => {
        this.products = products;
        this.cartItems = cart.items;
      });
    });
  }

  public ngOnDestroy(): void {
    if (this.cartSubscription) {
      this.cartSubscription.unsubscribe();
    }
  }

}
