import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../entities/product';
import { CartService } from '../services/cart.service';
import { ProductService } from '../services/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
import { ColorSelection } from '../entities/color.selection';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  product: Product;

  constructor(private cartService: CartService,
    private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      let productId = Number(params.get('productId'));
      this.product = this.productService.getProduct(productId);
    });
    this.route.queryParams.filter(params=>params.color)
    .subscribe(params=>{
      this.product.selectedColor = params.color;
    })

  }
  onColorSelected(colorSelection: ColorSelection){
    this.router.navigate(['/product',this.product.id], { queryParams: { color: colorSelection.color } });
  }
  public addProductToCart(): void {

    this.cartService.addItem(this.product, 1);
  }

  changeSize(selectedSize: string){
    this.product.selectedSize= selectedSize;
    this.product.price = this.product.prices.get(selectedSize);
  }


}
