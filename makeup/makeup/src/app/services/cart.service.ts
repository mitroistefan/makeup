import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from "rxjs/Observer";
import { Product } from '../entities/product';
import { ShoppingCartItem } from '../entities/shopping.cart.item';
import { ProductService } from '../services/product.service';
import { ShoppingCart } from '../entities/shopping.cart';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
const CART_KEY = "cart";


@Injectable()
export class CartService {
  private storage: Storage;
  private subscriptionObservable: Observable<ShoppingCart>;
  private subscribers: Array<Observer<ShoppingCart>> = new Array<Observer<ShoppingCart>>();
  private products: Product[];

  public constructor(private productService: ProductService) {
    this.storage = localStorage;
    this.productService.getProducts().subscribe((products) => this.products = products);
    this.subscriptionObservable = new Observable<ShoppingCart>((observer: Observer<ShoppingCart>) => {
      this.subscribers.push(observer);
      observer.next(this.retrieve());
      return () => {
        this.subscribers = this.subscribers.filter((obs) => obs !== observer);
      };
    });
  }

  public get(): Observable<ShoppingCart> {
    return this.subscriptionObservable;
  }

  public addItem(product: Product, quantity: number): void {
    const cart = this.retrieve();
    let item = cart.items.find((p) => p.product.id === product.id && p.product.selectedSize===
    product.selectedSize && p.product.selectedColor ===product.selectedColor);
    if (item === undefined) {
      item = new ShoppingCartItem();
      item.product = product;
      cart.items.push(item);
    }

    item.quantity += quantity;
    cart.items = cart.items.filter((cartItem) => cartItem.quantity > 0);

    this.calculateCart(cart);
    this.save(cart);
    this.dispatch(cart);
  }
  public updateQuantity(product: Product, quantity: number): void {
    const cart = this.retrieve();
    const  item = cart.items.find((p) => p.product.id === product.id);
    item.quantity -= item.quantity;
    item.quantity += quantity;
    this.calculateCart(cart);
    this.save(cart);
    this.dispatch(cart);
  }



  public empty(): void {
    const newCart = new ShoppingCart();
    this.save(newCart);
    this.dispatch(newCart);
  }



  private calculateCart(cart: ShoppingCart): void {
    cart.itemsTotal = cart.items
      .map((item) => item.quantity * Number(this.products.find((p) => p.id === item.product.id).price))
      .reduce((previous, current) => previous + current, 0);

    cart.grossTotal = cart.itemsTotal + cart.deliveryTotal;
    cart.itemCount = cart.items.map((x) => x.quantity).reduce((p, n) => p + n, 0);

  }

  private retrieve(): ShoppingCart {
    const cart = new ShoppingCart();
    const storedCart = this.storage.getItem(CART_KEY);
    if (storedCart) {
      cart.updateFrom(JSON.parse(storedCart));
    }

    return cart;
  }

  private save(cart: ShoppingCart): void {

    this.storage.setItem(CART_KEY, JSON.stringify(cart));
  }

  private dispatch(cart: ShoppingCart): void {
    this.subscribers
      .forEach((sub) => {
        try {
          sub.next(cart);
        } catch (e) {
          // we want all subscribers to get the update even if one errors.
        }
      });
  }

}
