import { Injectable } from '@angular/core';

import { Product } from '../entities/product';
import { ProductOverview } from '../entities/product.overview';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class ProductService {
  constructor() {
  }

  getProducts(): Observable<Product[]> {
    let products: Product[] = [
      { id: '1', name: 'name 1', colours:["red","green"], sizes: ["100X200","210X220","500X600"],
       description: 'desc', price: "100", prices: new Map([
    [ "100X200", "100" ],
    [ "210X220", "200" ],
    [ "500X600", "300" ]])
  },
      { id: '2', name: 'name 2', colours:["red","green"], sizes: ["100X200","210X220","500X600"],
       description: 'desc', price: "200",
       prices: new Map([
         [ "100X200", "100" ],
         [ "210X220", "200" ],
         [ "500X600", "300" ]])
     },
      { id: '3', name: 'name 3', colours:["red","green"], sizes: ["100X200","210X220","500X600"],
       description: 'desc', price: "300",prices: new Map([
         [ "100X200", "100" ],
         [ "210X220", "200" ],
         [ "500X600", "300" ]])
     },
      { id: '4', name: 'name 4', colours:["red","green"], sizes: ["100X200","210X220","500X600"],
       description: 'desc', price: "400",
       prices: new Map([
         [ "100X200", "100" ],
         [ "210X220", "200" ],
         [ "500X600", "300" ]])
     },
      { id: '5', name: 'name 5', colours:["red","green"], sizes: ["100X200","210X220","500X600"],
       description: 'desc', price: "500"
       ,prices: new Map([
         [ "100X200", "100" ],
         [ "210X220", "200" ],
         [ "500X600", "300" ]])
     }
    ];
    return Observable.of(products);
  }

<<<<<<< HEAD
    constructor() {
        this.products = [
            { id: 'p01', name: 'name 1', price: 100 },
            { id: 'p02', name: 'name 2', price: 200 },
            { id: 'p02', name: 'name 3', price: 210 },
            { id: 'p02', name: 'name 4', price: 200 },
            { id: 'p02', name: 'name 5', price: 240 },
            { id: 'p02', name: 'name 6', price: 500 },
            { id: 'p02', name: 'name 7', price: 200 },
            { id: 'p03', name: 'name 8', price: 300 }
        ];
    }
=======
>>>>>>> add_material

  getProductsList(): Observable<ProductOverview[]>{
    let products: ProductOverview[] = [
      { id: '1', name: 'name 1', priceRange: "100", colours:["red","black"] },
      { id: '2', name: 'name 2',  priceRange: "100", colours:["red","black"]},
      { id: '3', name: 'name 3',  priceRange: "100" ,colours:["red","black"]},
      { id: '4', name: 'name 4',  priceRange: "100" ,colours:["red","black"] },
      { id: '5', name: 'name 5',  priceRange: "100" ,colours:["red","black"]}
    ];
    return Observable.of(products);
  }

  getProduct(id: number): Product {
    if (id == 1) {
      return { id: '1', name: 'name 1', colours:["red","green"], sizes: ["100X200","210X220","500X600"], description: 'desc', price: "100" ,prices: new Map([
        [ "100X200", "100" ],
        [ "210X220", "200" ],
        [ "500X600", "300" ]])};
    }
    if (id == 2) {
      return { id: '2', name: 'name 2', colours:["red","green"], sizes: ["100X200","210X220","500X600"], description: 'desc', price: "200",prices: new Map([
        [ "100X200", "100" ],
        [ "210X220", "200" ],
        [ "500X600", "300" ]])};
    }
    if (id == 3) {
      return { id: '3', name: 'name 3', colours:["red","green"], sizes: ["100X200","210X220","500X600"], description: 'desc', price: "300" ,prices: new Map([
        [ "100X200", "100" ],
        [ "210X220", "200" ],
        [ "500X600", "300" ]])};
    }
    if (id == 4) {
      return { id: '4', name: 'name 4', colours:["red","green"], sizes: ["100X200","210X220","500X600"], description: 'desc', price: "400" ,prices: new Map([
        [ "100X200", "100" ],
        [ "210X220", "200" ],
        [ "500X600", "300" ]])};
    }
    if (id == 5) {
      return { id: '5', name: 'name 5', colours:["red","green"], sizes: ["100X200","210X220","500X600"], description: 'desc', price: "500" ,prices: new Map([
        [ "100X200", "100" ],
        [ "210X220", "200" ],
        [ "500X600", "300" ]])};
    }
  }
}
