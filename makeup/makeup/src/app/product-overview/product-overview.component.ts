import { Component, OnInit, Input } from '@angular/core';
import { ProductOverview } from '../entities/product.overview';
import { Router, ActivatedRoute } from '@angular/router';
import { ColorSelection } from '../entities/color.selection';

@Component({
  selector: 'app-product-overview',
  templateUrl: './product-overview.component.html',
  styleUrls: ['./product-overview.component.css']
})
export class ProductOverviewComponent implements OnInit {

  @Input()
  product: ProductOverview;
  colorSelection: ColorSelection;
  constructor(private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.product.selectedColor = this.product.colours[0];
  }
  onColorSelected(colorSelection: ColorSelection) {
    console.log("onColorSelected + ProductOverviewComponent")
    this.product.selectedColor = colorSelection.color;
  }
  showDetails(): void {
    this.router.navigate(['/product', this.product.id], { queryParams: { color: this.product.selectedColor } });
  }

}
