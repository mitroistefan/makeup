export interface ProductOverview {

    id: string;
    name: string;
    priceRange: string;
    colours: string[];
    selectedColor?: string;

}
