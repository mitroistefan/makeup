import { ShoppingCartItem } from './shopping.cart.item';

export class ShoppingCart {

  public items: ShoppingCartItem[] = new Array<ShoppingCartItem>();
  public deliveryOptionId: string;
  public grossTotal: number = 0;
  public deliveryTotal: number = 0;
  public itemsTotal: number = 0;
  public itemCount: number = 0;

  public updateFrom(src: ShoppingCart) {
    this.items = src.items;
    this.deliveryOptionId = src.deliveryOptionId;
    this.grossTotal = src.grossTotal;
    this.deliveryTotal = src.deliveryTotal;
    this.itemsTotal = src.itemsTotal;
    this.itemCount = src.itemCount;
  }

}
