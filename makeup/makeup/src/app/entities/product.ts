export class Product {

  id: string;
  name: string;
  colours: string[];
  sizes: string[];
  description: string;
  prices: Map<string, string>;
  price: string;
  selectedColor?: string;
  selectedSize?:string;

  // public updateFrom(src: Product): void {
  //   this.id = src.id;
  //   this.name = src.name;
  //   this.description = src.description;
  //   this.price = src.price;
  //
  // }
}
