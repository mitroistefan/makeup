import { Product } from './product';

export class ShoppingCartItem {
    public product: Product;
    public quantity: number = 0;
}
