import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ColorSelection } from '../entities/color.selection';

@Component({
  selector: 'app-color-selector',
  templateUrl: './color-selector.component.html',
  styleUrls: ['./color-selector.component.css']
})
export class ColorSelectorComponent implements OnInit {

  @Input()
  color: string;
  @Input()
  productId: string;
  @Output()
  colorSelected = new EventEmitter<ColorSelection>();

  constructor() { }

  ngOnInit() {

  }
  selectColor() {
    let colorSelection = new ColorSelection();
    colorSelection.productId = this.productId;
    colorSelection.color = this.color;
    this.colorSelected.emit(colorSelection);
  }

}
