import { Component, OnInit, Input } from '@angular/core';
import { ShoppingCartItem } from '../entities/shopping.cart.item';
import { CartService } from '../services/cart.service';
import { Product } from '../entities/product';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit {


  @Input()
  cartItem: ShoppingCartItem;
  quantities: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  constructor(private cartService: CartService) { }

  ngOnInit() {
  }
  removeProduct(product: Product) {
    this.cartService.addItem(product, - this.cartItem.quantity);
  }
  onQuantityChange(product: Product, searchValue: number) {
    // console.log(productId + " " + searchValue);
    this.cartService.updateQuantity(product, searchValue);
  }
}
