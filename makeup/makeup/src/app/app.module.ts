import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { StoreComponent } from './store/store.component';
import { ProductsComponent } from './products/products.component';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { CartComponent } from './cart/cart.component';
import { ProductService } from './services/product.service';
import {CartService} from './services/cart.service';
import {MatChipsModule} from '@angular/material/chips';
import {HttpModule} from '@angular/http';
import {MatInputModule} from '@angular/material/input';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatToolbarModule} from '@angular/material';
import { MatSidenavModule} from '@angular/material';
import { MatListModule} from '@angular/material';
import { MatButtonModule} from '@angular/material';
import { MatIconModule} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatBadgeModule} from '@angular/material/badge';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ProductComponent } from './product/product.component';
import {MatSelectModule} from '@angular/material/select';
import {MatDividerModule} from '@angular/material/divider';
import { CartItemComponent } from './cart-item/cart-item.component';
import { ColorSelectorComponent } from './color-selector/color-selector.component';
import { ProductOverviewComponent } from './product-overview/product-overview.component';

@NgModule({
    declarations: [
        AppComponent,
        IndexComponent,
        StoreComponent,
        ProductsComponent,
        FooterComponent,
        HeaderComponent,
        CartComponent,
        ProductComponent,
        CartItemComponent,
        ColorSelectorComponent,
        ProductOverviewComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatSidenavModule,
        MatChipsModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        MatBadgeModule,
        MatInputModule,
        MatSelectModule,
        MatCardModule,
        MatGridListModule,
        MatFormFieldModule,
        FlexLayoutModule,
        MatDividerModule,
        AppRoutingModule, FormsModule,
        HttpModule
    ],
    providers: [ProductService,CartService],
    bootstrap: [AppComponent]
})
export class AppModule { }
