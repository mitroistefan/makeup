import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../services/cart.service';
import { Observable } from 'rxjs/Observable';
import { ShoppingCart } from '../entities/shopping.cart';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isCollapsed = true;
  itemsNumber: number;

  constructor(private router: Router,
    private cartService: CartService) { }

  ngOnInit() {
    this.cartService.get().subscribe(res=>{
      this.itemsNumber = res.itemCount;
    })
  }

  getStore = function() {
    this.router.navigateByUrl('/store');
  };
  getProduct = function() {
    this.router.navigateByUrl('/product');
  };

}
